import React from 'react';

class AboutPage extends React.Component {

  render() {
    return (
      <div>
        <h1>About</h1>
        <p>This application uses React Redux ES6 to build the App admin Panel</p>
      </div>
    );
  }
}

export default AboutPage;
