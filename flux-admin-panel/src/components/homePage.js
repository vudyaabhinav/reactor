"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;

var Home = React.createClass({
  render: function() {
    return (
      <div className="jumbotron">
        <h1>Administration</h1>
        <p>
          React, React-Router, Flux for ultra-responsive web apps.
          <Link to="about" className="btn btn-primary">Learn More</Link>
        </p>
      </div>
    );
  }
});

module.exports = Home;
