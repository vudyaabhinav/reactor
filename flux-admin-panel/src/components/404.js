"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;

var NotFound = React.createClass({
  render: function() {
    return (
      <div className="jumbotron">
        <h1>Page Not Found</h1>
        <p>Oops! Something went wrong, there is nothing to see here.</p>
        <Link to="app" className="btn btn-warning">Back to home</Link>
      </div>
    );
  }
});

module.exports = NotFound;
