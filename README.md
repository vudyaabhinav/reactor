# Reactor
Reactor is a platform for implementing practice react projects. These projects could be run individually or run the platform and access each individual project through it (Coming soon)

## Tech Stack

Tech stack for this platform varies per each practice application inside, but a the common technologies used would be: 
- React
- Node
- Webpack
